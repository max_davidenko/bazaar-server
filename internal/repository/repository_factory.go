package repository

import (
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"strings"
)

// CreateRepository creates new repository with specified params
func CreateRepository(params map[string]string) (Repository, error) {
	repoName := params["repoName"]
	if strings.EqualFold(repoName, "postgres") {
		return newPostgresRepository(params)
	}

	return nil, errors.New("unknown repository name or name is not specified")
}

// newPostgresRepository - creates new instance of postgres repository
func newPostgresRepository(params map[string]string) (Repository, error) {
	pgUser, ok := params["user"]
	if !ok {
		return nil, errors.New("postgres user must be specified")
	}
	pgPass, ok := params["password"]
	if !ok {
		return nil, errors.New("postgres password must be specified")
	}
	pgHost, ok := params["host"]
	if !ok {
		return nil, errors.New("postgres host must be specified")
	}
	pgDb, ok := params["db"]
	if !ok {
		return nil, errors.New("postgres database must be specified")
	}
	pgSslMode, ok := params["sslmode"]
	if !ok {
		pgSslMode = "disable"
	}
	logMode, ok := params["logmode"]
	if !ok {
		logMode = "true"
	}
	url := fmt.Sprintf(
		"postgres://%s:%s@%s/%s?sslmode=%s",
		pgUser, pgPass, pgHost, pgDb, pgSslMode,
	)
	db, err := gorm.Open("postgres", url)
	if err != nil {
		return nil, err
	}
	db.LogMode(strings.EqualFold("true", logMode))

	return &PostgresRepository{db}, nil
}
