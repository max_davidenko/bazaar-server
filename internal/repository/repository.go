package repository

import "bitbucket.org/max_davidenko/bazaar-server/internal/model"

type Repository interface {
	InitModel(modelItems ...interface{}) error

	// Area
	GetAreas() ([]model.Area, error)
	GetArea(uint) (*model.Area, error)
	FindAreaByName(string) (*model.Area, error)
	CreateArea(*model.Area) error
	UpdateArea(*model.Area) error
	DeleteArea(*model.Area) error
	GetAreaRows(uint) ([]model.Row, error)
	FindAreaRowByName(uint, string) (*model.Row, error)

	// Row
	GetRow(uint) (*model.Row, error)
	CreateRow(*model.Row) error
	UpdateRow(*model.Row) error
	DeleteRow(*model.Row) error
	GetRowPlaces(uint) ([]model.Place, error)
	FindRowPlaceByName(uint, string) (*model.Place, error)

	// Place
	GetPlace(uint) (*model.Place, error)
	CreatePlace(*model.Place) error
	UpdatePlace(*model.Place) error
	DeletePlace(*model.Place) error
	GetPlaceAttributes(uint) ([]model.PlaceAttribute, error)
	FindPlaceAttributeByName(uint, string) (*model.PlaceAttribute, error)
	GetPlaceAttribute(uint) (*model.PlaceAttribute, error)
	CreatePlaceAttribute(*model.PlaceAttribute) error
	UpdatePlaceAttribute(*model.PlaceAttribute) error
	DeletePlaceAttribute(*model.PlaceAttribute) error
	GetPlaceTariffs(uint) ([]model.PlaceTariff, error)
	FindTariffToPlaceBinding(uint, uint) (*model.PlaceTariff, error)
	GetPlaceTariff(uint) (*model.PlaceTariff, error)
	CreatePlaceTariff(*model.PlaceTariff) error
	DeletePlaceTariff(*model.PlaceTariff) error

	// Tariff
	GetTariffs() ([]model.Tariff, error)
	GetTariff(uint) (*model.Tariff, error)
	FindTariffByName(string) (*model.Tariff, error)
	CreateTariff(*model.Tariff) error
	UpdateTariff(*model.Tariff) error
	DeleteTariff(*model.Tariff) error

	// Unit
	GetUnits() ([]model.Unit, error)
	GetUnit(uint) (*model.Unit, error)
	FindUnitByName(string) (*model.Unit, error)
	CreateUnit(*model.Unit) error
	UpdateUnit(*model.Unit) error
	DeleteUnit(*model.Unit) error

	// Customer
	GetCustomers() ([]model.Customer, error)
	GetCustomer(uint) (*model.Customer, error)
	FindCustomerByINN(string) (*model.Customer, error)
	CreateCustomer(*model.Customer) error
	UpdateCustomer(*model.Customer) error
	DeleteCustomer(*model.Customer) error
}
