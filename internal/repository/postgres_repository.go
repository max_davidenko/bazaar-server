package repository

import (
	"bitbucket.org/max_davidenko/bazaar-server/internal/model"
	"github.com/jinzhu/gorm"
)

// PostgresRepository implements repository interface with connection to postgres
type PostgresRepository struct {
	db *gorm.DB
}

// InitModel performs model initialization via gorm
func (repo *PostgresRepository) InitModel(modelItems ...interface{}) error {
	return repo.db.AutoMigrate(modelItems...).Error
}

// GetAreas returns all areas from the database
func (repo *PostgresRepository) GetAreas() ([]model.Area, error) {
	areas := []model.Area{}
	if err := repo.db.Find(&areas).Error; err != nil {
		return nil, err
	}

	if len(areas) == 0 {
		return nil, &NotFoundError{"areas"}
	}

	return areas, nil
}

// GetArea returns area object with specified id from the database
func (repo *PostgresRepository) GetArea(id uint) (*model.Area, error) {
	area := &model.Area{}
	if err := repo.db.First(area, id).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &NotFoundError{"area"}
		}
		return nil, err
	}

	return area, nil
}

// FindAreaByName performs area object search in the database by specified name
func (repo *PostgresRepository) FindAreaByName(name string) (*model.Area, error) {
	area := &model.Area{Name: name}
	if err := repo.db.Where(area).First(area).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &NotFoundError{"area"}
		}
		return nil, err
	}

	return area, nil
}

// CreateArea creates new area record in the database
func (repo *PostgresRepository) CreateArea(area *model.Area) error {
	return repo.db.Create(area).Error
}

// UpdateArea updates specified area record in the database
func (repo *PostgresRepository) UpdateArea(area *model.Area) error {
	return repo.db.Model(area).Updates(area).Error
}

// DeleteArea deletes specified area record from the database
func (repo *PostgresRepository) DeleteArea(area *model.Area) error {
	return repo.db.Delete(area).Error
}

// GetAreaRows returns rows list associated with specified area
func (repo *PostgresRepository) GetAreaRows(areaID uint) ([]model.Row, error) {
	rows := []model.Row{}
	if err := repo.db.Where(&model.Row{AreaID: areaID}).Find(&rows).Error; err != nil {
		return nil, err
	}

	if len(rows) == 0 {
		return nil, &NotFoundError{"area rows"}
	}

	return rows, nil
}

// FindAreaRowByName performs row object search in the database by specified name and area id
func (repo *PostgresRepository) FindAreaRowByName(areaID uint, name string) (*model.Row, error) {
	row := &model.Row{AreaID: areaID, Name: name}
	if err := repo.db.Where(row).First(row).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &NotFoundError{"row"}
		}
		return nil, err
	}

	return row, nil
}

// GetRow returns row object with specified id from the database
func (repo *PostgresRepository) GetRow(id uint) (*model.Row, error) {
	row := &model.Row{}
	if err := repo.db.First(row, id).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &NotFoundError{"row"}
		}
		return nil, err
	}

	return row, nil
}

// CreateRow creates new row record in the database
func (repo *PostgresRepository) CreateRow(row *model.Row) error {
	return repo.db.Create(row).Error
}

// UpdateRow updates specified row record in the database
func (repo *PostgresRepository) UpdateRow(row *model.Row) error {
	return repo.db.Model(row).Updates(row).Error
}

// DeleteRow deletes specified row record from the database
func (repo *PostgresRepository) DeleteRow(row *model.Row) error {
	return repo.db.Delete(row).Error
}

// GetRowPlaces gets places with specified row id from the database
func (repo *PostgresRepository) GetRowPlaces(rowId uint) ([]model.Place, error) {
	places := []model.Place{}
	if err := repo.db.Where(&model.Place{RowID: rowId}).Find(&places).Error; err != nil {
		return nil, err
	}

	if len(places) == 0 {
		return nil, &NotFoundError{"row places"}
	}

	return places, nil
}

// FindRowPlaceByName performs place object search in the database by specified name and row id
func (repo *PostgresRepository) FindRowPlaceByName(rowID uint, name string) (*model.Place, error) {
	place := &model.Place{RowID: rowID, Name: name}
	if err := repo.db.Where(place).First(place).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &NotFoundError{"place"}
		}
		return nil, err
	}

	return place, nil
}

// GetPlace gets place with specified id from the database
func (repo *PostgresRepository) GetPlace(id uint) (*model.Place, error) {
	place := &model.Place{}
	if err := repo.db.First(place, id).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &NotFoundError{"place"}
		}
		return nil, err
	}

	return place, nil
}

// CreatePlace creates new place record in the database
func (repo *PostgresRepository) CreatePlace(place *model.Place) error {
	return repo.db.Create(place).Error
}

// UpdatePlace updates specified place record in the database
func (repo *PostgresRepository) UpdatePlace(place *model.Place) error {
	return repo.db.Model(place).Updates(place).Error
}

// DeletePlace deletes specified place record from the database
func (repo *PostgresRepository) DeletePlace(place *model.Place) error {
	return repo.db.Delete(place).Error
}

// GetPlaceAttributes gets attributes of specified place from the database
func (repo *PostgresRepository) GetPlaceAttributes(placeId uint) ([]model.PlaceAttribute, error) {
	attrs := []model.PlaceAttribute{}
	if err := repo.db.Where(&model.PlaceAttribute{PlaceID: placeId}).Find(&attrs).Error; err != nil {
		return nil, err
	}

	if len(attrs) == 0 {
		return nil, &NotFoundError{"place attributes"}
	}

	return attrs, nil
}

// FindPlaceAttributeByName performs place attribute object search in the database by specified name and place id
func (repo *PostgresRepository) FindPlaceAttributeByName(placeId uint, name string) (*model.PlaceAttribute, error) {
	attr := &model.PlaceAttribute{PlaceID: placeId, Name: name}
	if err := repo.db.Where(attr).First(attr).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &NotFoundError{"place attribute"}
		}
		return nil, err
	}

	return attr, nil
}

// GetPlaceAttribute gets place attribute with specified id from the database
func (repo *PostgresRepository) GetPlaceAttribute(id uint) (*model.PlaceAttribute, error) {
	attr := &model.PlaceAttribute{}
	if err := repo.db.First(attr, id).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &NotFoundError{"place attribute"}
		}
		return nil, err
	}

	return attr, nil
}

// CreatePlaceAttribute creates new place attribute record in the database
func (repo *PostgresRepository) CreatePlaceAttribute(attr *model.PlaceAttribute) error {
	return repo.db.Create(attr).Error
}

// UpdatePlaceAttribute updates specified place attribute record in the database
func (repo *PostgresRepository) UpdatePlaceAttribute(attr *model.PlaceAttribute) error {
	return repo.db.Model(attr).Updates(attr).Error
}

// DeletePlaceAttribute deletes specified place attribute record from the database
func (repo *PostgresRepository) DeletePlaceAttribute(attr *model.PlaceAttribute) error {
	return repo.db.Delete(attr).Error
}

// GetTariffs returns all tariffs records from the database
func (repo *PostgresRepository) GetTariffs() ([]model.Tariff, error) {
	tariffs := []model.Tariff{}
	if err := repo.db.Find(&tariffs).Error; err != nil {
		return nil, err
	}

	if len(tariffs) == 0 {
		return nil, &NotFoundError{"tariffs"}
	}

	return tariffs, nil
}

// GetTariff gets tariff with specified id from the database
func (repo *PostgresRepository) GetTariff(id uint) (*model.Tariff, error) {
	tariff := &model.Tariff{}
	if err := repo.db.First(tariff, id).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &NotFoundError{"tariff"}
		}
		return nil, err
	}

	return tariff, nil
}

// FindTariffByName performs tariff object search in the database by specified name
func (repo *PostgresRepository) FindTariffByName(name string) (*model.Tariff, error) {
	tariff := &model.Tariff{Name: name}
	if err := repo.db.Where(tariff).First(tariff).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &NotFoundError{"tariff"}
		}
		return nil, err
	}

	return tariff, nil
}

// CreateTariff creates new tariff record in the database
func (repo *PostgresRepository) CreateTariff(tariff *model.Tariff) error {
	return repo.db.Create(tariff).Error
}

// UpdateTariff updates specified tariff record in the database
func (repo *PostgresRepository) UpdateTariff(tariff *model.Tariff) error {
	return repo.db.Model(tariff).Updates(tariff).Error
}

// DeleteTariff deletes specified tariff record from the database
func (repo *PostgresRepository) DeleteTariff(tariff *model.Tariff) error {
	return repo.db.Delete(tariff).Error
}

// GetUnits returns all unit records from the database
func (repo *PostgresRepository) GetUnits() ([]model.Unit, error) {
	units := []model.Unit{}
	if err := repo.db.Find(&units).Error; err != nil {
		return nil, err
	}

	if len(units) == 0 {
		return nil, &NotFoundError{"units"}
	}

	return units, nil
}

// GetUnit returns measure unit record with specified id from the database
func (repo *PostgresRepository) GetUnit(id uint) (*model.Unit, error) {
	unit := &model.Unit{}
	if err := repo.db.First(unit, id).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &NotFoundError{"unit"}
		}
		return nil, err
	}

	return unit, nil
}

// FindTariffByName performs unit object search in the database by specified name
func (repo *PostgresRepository) FindUnitByName(name string) (*model.Unit, error) {
	unit := &model.Unit{Name: name}
	if err := repo.db.Where(unit).First(unit).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &NotFoundError{"unit"}
		}
		return nil, err
	}

	return unit, nil
}

// CreateTariff creates new unit record in the database
func (repo *PostgresRepository) CreateUnit(unit *model.Unit) error {
	return repo.db.Create(unit).Error
}

// UpdateTariff updates specified unit record in the database
func (repo *PostgresRepository) UpdateUnit(unit *model.Unit) error {
	return repo.db.Model(unit).Updates(unit).Error
}

// DeleteTariff deletes specified unit record from the database
func (repo *PostgresRepository) DeleteUnit(unit *model.Unit) error {
	return repo.db.Delete(unit).Error
}

// GetPlaceTariffs gets tariffs bindings for specified place from the database
func (repo *PostgresRepository) GetPlaceTariffs(placeId uint) ([]model.PlaceTariff, error) {
	bindings := []model.PlaceTariff{}
	if err := repo.db.Where(&model.PlaceTariff{PlaceID: placeId}).Find(&bindings).Error; err != nil {
		return nil, err
	}

	if len(bindings) == 0 {
		return nil, &NotFoundError{"place tariffs"}
	}

	return bindings, nil
}

// FindTariffToPlaceBinding performs binding object search for specified place and tariff in the database
func (repo *PostgresRepository) FindTariffToPlaceBinding(placeId uint, tariffId uint) (*model.PlaceTariff, error) {
	binding := &model.PlaceTariff{PlaceID: placeId, TariffID: tariffId}
	if err := repo.db.Where(binding).First(binding).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &NotFoundError{"place tariff"}
		}
		return nil, err
	}

	return binding, nil
}

// GetPlaceTariff returns place to tariff binding record with specified id from the database
func (repo *PostgresRepository) GetPlaceTariff(id uint) (*model.PlaceTariff, error) {
	binding := &model.PlaceTariff{}
	if err := repo.db.First(binding, id).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &NotFoundError{"place tariff"}
		}
		return nil, err
	}

	return binding, nil
}

// CreatePlaceTariff creates new tariff to place binding record in the database
func (repo *PostgresRepository) CreatePlaceTariff(binding *model.PlaceTariff) error {
	return repo.db.Create(binding).Error
}

// DeletePlaceTariff deletes specified tariff to place binding record from the database
func (repo *PostgresRepository) DeletePlaceTariff(binding *model.PlaceTariff) error {
	return repo.db.Delete(binding).Error
}

// GetCustomers returns all customer records from the database
func (repo *PostgresRepository) GetCustomers() ([]model.Customer, error) {
	customers := []model.Customer{}
	if err := repo.db.Find(&customers).Error; err != nil {
		return nil, err
	}

	if len(customers) == 0 {
		return nil, &NotFoundError{"customers"}
	}

	return customers, nil
}

// GetCustomer gets customer with specified id from the database
func (repo *PostgresRepository) GetCustomer(id uint) (*model.Customer, error) {
	customer := &model.Customer{}
	if err := repo.db.First(customer, id).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &NotFoundError{"customer"}
		}
		return nil, err
	}

	return customer, nil
}

// FindCustomerByINN
func (repo *PostgresRepository) FindCustomerByINN(inn string) (*model.Customer, error) {
	customer := &model.Customer{Inn: inn}
	if err := repo.db.Where(customer).First(customer).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &NotFoundError{"customer"}
		}
		return nil, err
	}

	return customer, nil
}

// CreateCustomer creates new customer record in the database
func (repo *PostgresRepository) CreateCustomer(customer *model.Customer) error {
	return repo.db.Create(customer).Error
}

// UpdateCustomer updates specified customer record in the database
func (repo *PostgresRepository) UpdateCustomer(customer *model.Customer) error {
	return repo.db.Model(customer).Updates(customer).Error
}

// DeleteCustomer deletes specified customer record from the database
func (repo *PostgresRepository) DeleteCustomer(customer *model.Customer) error {
	return repo.db.Delete(customer).Error
}
