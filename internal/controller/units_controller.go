package controller

import (
	"bitbucket.org/max_davidenko/bazaar-server/internal/model"
	"bitbucket.org/max_davidenko/bazaar-server/internal/repository"
	"net/http"
	"strings"
)

// GetUnits is a handler for GET /units
func (ctrl *Controller) GetUnits(response http.ResponseWriter, request *http.Request) {
	units, err := ctrl.repo.GetUnits()
	writeBasicHeaders(response)
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	unitsList := make(map[string][]model.Unit)
	unitsList["units"] = units
	writeResponse(response, unitsList)
}

// GetUnit is a handler for GET /unit/{id}
func (ctrl *Controller) GetUnit(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	unitId, err := varAsInt(request, "id", "unit")
	if err != nil {
		writeError(response, err)
		return
	}

	unit, err := ctrl.repo.GetUnit(uint(unitId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}
	writeResponse(response, unit)
}

// CreateUnit is a handler for POST /units
func (ctrl *Controller) CreateUnit(response http.ResponseWriter, request *http.Request) {

	unit := &model.Unit{}
	if !unmarshalBody(request, response, unit) {
		return
	}

	unit.Name = strings.TrimSpace(unit.Name)
	if unit.Name == "" {
		writeError(response, newBadRequestErrorFromString("unit name must be specified"))
		return
	}

	foundUnit, err := ctrl.repo.FindUnitByName(unit.Name)
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); !isNotFound {
			writeError(response, err)
			return
		}
	}
	if foundUnit != nil {
		writeError(response, newBadRequestErrorFromString("Unit with specified name exists"))
		return
	}

	err = ctrl.repo.CreateUnit(unit)
	if err != nil {
		writeError(response, err)
		return
	}

	writeResponse(response, unit)
}

// EditUnit is a handler for PUT /units/{id}
func (ctrl *Controller) EditUnit(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	unitId, err := varAsInt(request, "id", "unit")
	if err != nil {
		writeError(response, err)
		return
	}

	unit, err := ctrl.repo.GetUnit(uint(unitId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	updUnit := &model.Unit{}
	if !unmarshalBody(request, response, updUnit) {
		return
	}

	updUnit.ID = unit.ID
	updUnit.Name = strings.TrimSpace(updUnit.Name)
	if updUnit.Name != "" {
		foundUnit, err := ctrl.repo.FindUnitByName(updUnit.Name)
		if err != nil {
			if _, isNotFound := err.(*repository.NotFoundError); !isNotFound {
				writeError(response, err)
				return
			}
		}
		if foundUnit != nil && foundUnit.ID != updUnit.ID {
			writeError(response, newBadRequestErrorFromString("Unit with specified name exists"))
			return
		}

		err = ctrl.repo.UpdateUnit(updUnit)
		if err != nil {
			writeError(response, err)
			return
		}
	}

	response.WriteHeader(http.StatusNoContent)
}

// DeleteUnit is a handler for DELETE /unit/{id}
func (ctrl *Controller) DeleteUnit(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	unitId, err := varAsInt(request, "id", "unit")
	if err != nil {
		writeError(response, err)
		return
	}

	unit, err := ctrl.repo.GetUnit(uint(unitId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	err = ctrl.repo.DeleteUnit(unit)
	if err != nil {
		writeError(response, err)
		return
	}

	response.WriteHeader(http.StatusNoContent)
}
