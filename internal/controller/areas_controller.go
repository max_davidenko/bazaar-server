package controller

import (
	"bitbucket.org/max_davidenko/bazaar-server/internal/model"
	"bitbucket.org/max_davidenko/bazaar-server/internal/repository"
	"net/http"
	"strings"
)

// GetAreas is a handler for GET /areas
func (ctrl *Controller) GetAreas(response http.ResponseWriter, request *http.Request) {
	areas, err := ctrl.repo.GetAreas()
	writeBasicHeaders(response)
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	areasList := make(map[string][]model.Area)
	areasList["areas"] = areas
	writeResponse(response, areasList)
}

// GetArea is a handler for GET /areas/{id}
func (ctrl *Controller) GetArea(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	areaId, err := varAsInt(request, "id", "area")
	if err != nil {
		writeError(response, err)
		return
	}

	area, err := ctrl.repo.GetArea(uint(areaId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}
	writeResponse(response, area)
}

// CreateArea is a handler for POST /areas
func (ctrl *Controller) CreateArea(response http.ResponseWriter, request *http.Request) {

	area := &model.Area{}
	if !unmarshalBody(request, response, area) {
		return
	}

	area.Name = strings.TrimSpace(area.Name)
	if area.Name == "" {
		writeError(response, newBadRequestErrorFromString("area name must be specified"))
		return
	}

	foundArea, err := ctrl.repo.FindAreaByName(area.Name)
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); !isNotFound {
			writeError(response, err)
			return
		}
	}
	if foundArea != nil {
		writeError(response, newBadRequestErrorFromString("Area with specified name exists"))
		return
	}

	// TODO if controller ID != 0 check controller exists

	err = ctrl.repo.CreateArea(area)
	if err != nil {
		writeError(response, err)
		return
	}

	writeResponse(response, area)
}

// EditArea is a handler for PUT /areas/{id}
func (ctrl *Controller) EditArea(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	areaId, err := varAsInt(request, "id", "area")
	if err != nil {
		writeError(response, err)
		return
	}

	area, err := ctrl.repo.GetArea(uint(areaId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	updArea := &model.Area{}
	if !unmarshalBody(request, response, updArea) {
		return
	}

	updArea.ID = area.ID
	updArea.Name = strings.TrimSpace(updArea.Name)
	if updArea.Name != "" {
		foundArea, err := ctrl.repo.FindAreaByName(area.Name)
		if err != nil {
			if _, isNotFound := err.(*repository.NotFoundError); !isNotFound {
				writeError(response, err)
				return
			}
		}
		if foundArea != nil && foundArea.ID != updArea.ID {
			writeError(response, newBadRequestErrorFromString("Area with specified name exists"))
			return
		}
	}

	err = ctrl.repo.UpdateArea(updArea)
	if err != nil {
		writeError(response, err)
		return
	}

	response.WriteHeader(http.StatusNoContent)
}

// DeleteArea is a handler for DELETE /areas/{id}
func (ctrl *Controller) DeleteArea(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	areaId, err := varAsInt(request, "id", "area")
	if err != nil {
		writeError(response, err)
		return
	}

	area, err := ctrl.repo.GetArea(uint(areaId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	err = ctrl.repo.DeleteArea(area)
	if err != nil {
		writeError(response, err)
		return
	}

	response.WriteHeader(http.StatusNoContent)
}
