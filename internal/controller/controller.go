package controller

import (
	"bitbucket.org/max_davidenko/bazaar-server/internal/repository"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
)

// Controller contains link to instance of application's repository and methods responsible for incoming requests handling
type Controller struct {
	repo repository.Repository
}

// NewController creates new Controller instance with specified repository implementation
func NewController(repo repository.Repository) *Controller {
	return &Controller{repo}
}

// Handlers returns bindings for paths and handler functions
func (ctrl *Controller) Handlers() map[string]http.HandlerFunc {
	bindings := make(map[string]http.HandlerFunc)
	if ctrl == nil {
		return bindings
	}

	bindings["Get areas"] = ctrl.GetAreas
	bindings["Get area"] = ctrl.GetArea
	bindings["Create area"] = ctrl.CreateArea
	bindings["Edit area"] = ctrl.EditArea
	bindings["Delete area"] = ctrl.DeleteArea
	bindings["Get area rows"] = ctrl.GetAreaRows

	bindings["Get row"] = ctrl.GetRow
	bindings["Add row"] = ctrl.CreateRow
	bindings["Edit row"] = ctrl.EditRow
	bindings["Delete row"] = ctrl.DeleteRow

	bindings["Get row places"] = ctrl.GetRowPlaces
	bindings["Get place"] = ctrl.GetPlace
	bindings["Add place"] = ctrl.CreatePlace
	bindings["Edit place"] = ctrl.EditPlace
	bindings["Delete place"] = ctrl.DeletePlace

	bindings["Get tariffs"] = ctrl.GetTariffs
	bindings["Get tariff"] = ctrl.GetTariff
	bindings["Add tariff"] = ctrl.CreateTariff
	bindings["Edit tariff"] = ctrl.EditTariff
	bindings["Delete tariff"] = ctrl.DeleteTariff

	bindings["Get units"] = ctrl.GetUnits
	bindings["Get unit"] = ctrl.GetUnit
	bindings["Add unit"] = ctrl.CreateUnit
	bindings["Edit unit"] = ctrl.EditUnit
	bindings["Delete unit"] = ctrl.DeleteUnit

	bindings["Get place attributes"] = ctrl.GetPlacesAttributes
	bindings["Get place attribute"] = ctrl.GetPlaceAttribute
	bindings["Add place attribute"] = ctrl.CreatePlaceAttribute
	bindings["Edit place attribute"] = ctrl.EditPlaceAttribute
	bindings["Delete place attribute"] = ctrl.DeletePlaceAttribute

	bindings["Get place tariffs"] = ctrl.GetPlacesTariffs
	bindings["Add place tariff"] = ctrl.BindTariffToPlace
	bindings["Delete place tariff"] = ctrl.UnbindTariffFromPlace

	bindings["Get customers"] = ctrl.GetCustomers
	bindings["Get customer"] = ctrl.GetCustomer
	bindings["Add customer"] = ctrl.CreateCustomer
	bindings["Edit customer"] = ctrl.EditCustomer
	bindings["Delete customer"] = ctrl.DeleteCustomer

	return bindings
}

func writeBasicHeaders(rw http.ResponseWriter) {
	rw.Header().Set("Content-Type", "application/json; charset=UTF-8")
	rw.Header().Set("Access-Control-Allow-Origin", "*")
}

func writeResponse(rw http.ResponseWriter, bodyObj interface{}) {
	body, jsErr := json.Marshal(bodyObj)
	if jsErr != nil {
		writeError(rw, jsErr)
	} else {
		rw.WriteHeader(http.StatusOK)
		rw.Write(body)
	}
}

func writeError(rw http.ResponseWriter, err error) {
	// TODO log error
	errBody, jsErr := json.Marshal(newApiError(err))
	if jsErr != nil {
		rw.WriteHeader(http.StatusInternalServerError)
	} else {
		switch err.(type) {
		case badRequestError:
			rw.WriteHeader(http.StatusBadRequest)
		case notFoundError:
			rw.WriteHeader(http.StatusNotFound)
		default:
			rw.WriteHeader(http.StatusInternalServerError)
		}

		rw.Write(errBody)
	}
}

func unmarshalBody(request *http.Request, response http.ResponseWriter, out interface{}) bool {
	// TODO move limit to config
	body, err := ioutil.ReadAll(io.LimitReader(request.Body, 1048576))

	if err != nil {
		writeError(response, newInternalErrorFromString("Unable to read request body: "+err.Error()))
		return false
	}

	if err := request.Body.Close(); err != nil {
		writeError(response, newInternalErrorFromString("Unable to close request body: "+err.Error()))
		return false
	}

	if err := json.Unmarshal(body, out); err != nil {
		writeError(response, newInternalErrorFromString("Unable to unmarshal body: "+err.Error()))
		return false
	}

	return true
}

func varAsInt(request *http.Request, name string, what string) (int, error) {
	vars := mux.Vars(request)
	varStr, ok := vars[name]
	if !ok {
		return 0, newBadRequestErrorFromString(fmt.Sprintf("%s %s is not specified", what, name))
	}

	val, err := strconv.Atoi(varStr)
	if err != nil {
		return 0, newBadRequestErrorFromString(fmt.Sprintf("%s %s is not an integer", what, name))
	}

	return val, nil
}
