package controller

import (
	"bitbucket.org/max_davidenko/bazaar-server/internal/model"
	"bitbucket.org/max_davidenko/bazaar-server/internal/repository"
	"net/http"
	"strings"
)

// GetAreaRows is a handler for GET /areas/{id}/rows
func (ctrl *Controller) GetAreaRows(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	areaId, err := varAsInt(request, "id", "area")
	if err != nil {
		writeError(response, err)
		return
	}

	_, err = ctrl.repo.GetArea(uint(areaId))
	if err != nil {
		if err != nil {
			if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
				writeError(response, newNotFoundError(err))
			} else {
				writeError(response, err)
			}
			return
		}
	}

	rows, err := ctrl.repo.GetAreaRows(uint(areaId))
	writeBasicHeaders(response)
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	rowsList := make(map[string][]model.Row)
	rowsList["rows"] = rows
	writeResponse(response, rowsList)
}

// GetRow is a handler for GET /rows/{id}
func (ctrl *Controller) GetRow(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	rowId, err := varAsInt(request, "id", "row")
	if err != nil {
		writeError(response, err)
		return
	}

	row, err := ctrl.repo.GetRow(uint(rowId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}
	writeResponse(response, row)
}

// CreateRow is a handler for POST /rows
func (ctrl *Controller) CreateRow(response http.ResponseWriter, request *http.Request) {

	row := &model.Row{}
	if !unmarshalBody(request, response, row) {
		return
	}

	_, err := ctrl.repo.GetArea(uint(row.AreaID))
	if err != nil {
		if err != nil {
			if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
				writeError(response, newNotFoundError(err))
			} else {
				writeError(response, err)
			}
			return
		}
	}

	row.Name = strings.TrimSpace(row.Name)
	if row.Name == "" {
		writeError(response, newBadRequestErrorFromString("row name must be specified"))
		return
	}

	foundRow, err := ctrl.repo.FindAreaRowByName(row.AreaID, row.Name)
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); !isNotFound {
			writeError(response, err)
			return
		}
	}
	if foundRow != nil {
		writeError(response, newBadRequestErrorFromString("Area row with specified name exists"))
		return
	}

	err = ctrl.repo.CreateRow(row)
	if err != nil {
		writeError(response, err)
		return
	}

	writeResponse(response, row)
}

// EditRow is a handler for PUT /rows/{id}
func (ctrl *Controller) EditRow(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	rowId, err := varAsInt(request, "id", "row")
	if err != nil {
		writeError(response, err)
		return
	}

	row, err := ctrl.repo.GetRow(uint(rowId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	updRow := &model.Row{}
	if !unmarshalBody(request, response, updRow) {
		return
	}

	updRow.ID = row.ID
	updRow.AreaID = row.AreaID
	updRow.Name = strings.TrimSpace(updRow.Name)
	if updRow.Name != "" {
		foundRow, err := ctrl.repo.FindAreaRowByName(updRow.AreaID, updRow.Name)
		if err != nil {
			if _, isNotFound := err.(*repository.NotFoundError); !isNotFound {
				writeError(response, err)
				return
			}
		}
		if foundRow != nil && foundRow.ID != updRow.ID {
			writeError(response, newBadRequestErrorFromString("Row with specified name exists"))
			return
		}
	}

	err = ctrl.repo.UpdateRow(updRow)
	if err != nil {
		writeError(response, err)
		return
	}

	response.WriteHeader(http.StatusNoContent)
}

// DeleteRow is a handler for DELETE /rows/{id}
func (ctrl *Controller) DeleteRow(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	rowId, err := varAsInt(request, "id", "row")
	if err != nil {
		writeError(response, err)
		return
	}

	row, err := ctrl.repo.GetRow(uint(rowId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	err = ctrl.repo.DeleteRow(row)
	if err != nil {
		writeError(response, err)
		return
	}

	response.WriteHeader(http.StatusNoContent)
}
