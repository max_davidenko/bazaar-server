package controller

import (
	"bitbucket.org/max_davidenko/bazaar-server/internal/model"
	"bitbucket.org/max_davidenko/bazaar-server/internal/repository"
	"net/http"
	"strings"
)

// GetTariffs is a handler for GET /tariffs
func (ctrl *Controller) GetTariffs(response http.ResponseWriter, request *http.Request) {
	tariffs, err := ctrl.repo.GetTariffs()
	writeBasicHeaders(response)
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	tariffsList := make(map[string][]model.Tariff)
	tariffsList["tariffs"] = tariffs
	writeResponse(response, tariffsList)
}

// GetTariff is a handler for GET /tariffs/{id}
func (ctrl *Controller) GetTariff(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	tariffId, err := varAsInt(request, "id", "tariff")
	if err != nil {
		writeError(response, err)
		return
	}

	tariff, err := ctrl.repo.GetTariff(uint(tariffId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}
	writeResponse(response, tariff)
}

// CreateTariff is a handler for POST /tariffs
func (ctrl *Controller) CreateTariff(response http.ResponseWriter, request *http.Request) {

	tariff := &model.Tariff{}
	if !unmarshalBody(request, response, tariff) {
		return
	}

	tariff.Name = strings.TrimSpace(tariff.Name)
	if tariff.Name == "" {
		writeError(response, newBadRequestErrorFromString("tariff name must be specified"))
		return
	}

	foundTariff, err := ctrl.repo.FindTariffByName(tariff.Name)
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); !isNotFound {
			writeError(response, err)
			return
		}
	}
	if foundTariff != nil {
		writeError(response, newBadRequestErrorFromString("Tariff with specified name exists"))
		return
	}

	if tariff.Value == 0 {
		writeError(response, newBadRequestErrorFromString("Tariff value must be specified"))
		return
	}

	if tariff.UnitID != 0 {
		_, err := ctrl.repo.GetUnit(tariff.UnitID)
		if err != nil {
			if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
				writeError(response, newNotFoundError(err))
			} else {
				writeError(response, err)
			}
			return
		}
	}

	err = ctrl.repo.CreateTariff(tariff)
	if err != nil {
		writeError(response, err)
		return
	}

	writeResponse(response, tariff)
}

// EditTariff is a handler for PUT /tariffs/{id}
func (ctrl *Controller) EditTariff(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	tariffId, err := varAsInt(request, "id", "tariff")
	if err != nil {
		writeError(response, err)
		return
	}

	tariff, err := ctrl.repo.GetTariff(uint(tariffId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	updTariff := &model.Tariff{}
	if !unmarshalBody(request, response, updTariff) {
		return
	}

	updTariff.ID = tariff.ID
	updTariff.Name = strings.TrimSpace(updTariff.Name)
	if updTariff.Name != "" {
		foundTariff, err := ctrl.repo.FindTariffByName(updTariff.Name)
		if err != nil {
			if _, isNotFound := err.(*repository.NotFoundError); !isNotFound {
				writeError(response, err)
				return
			}
		}
		if foundTariff != nil && foundTariff.ID != updTariff.ID {
			writeError(response, newBadRequestErrorFromString("Tariff with specified name exists"))
			return
		}
	}

	if updTariff.UnitID != 0 && updTariff.UnitID != tariff.UnitID {
		_, err := ctrl.repo.GetUnit(tariff.UnitID)
		if err != nil {
			if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
				writeError(response, newNotFoundError(err))
			} else {
				writeError(response, err)
			}
			return
		}
	}

	err = ctrl.repo.UpdateTariff(updTariff)
	if err != nil {
		writeError(response, err)
		return
	}

	response.WriteHeader(http.StatusNoContent)
}

// DeleteTariff is a handler for DELETE /tariffs/{id}
func (ctrl *Controller) DeleteTariff(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	tariffId, err := varAsInt(request, "id", "tariff")
	if err != nil {
		writeError(response, err)
		return
	}

	tariff, err := ctrl.repo.GetTariff(uint(tariffId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	err = ctrl.repo.DeleteTariff(tariff)
	if err != nil {
		writeError(response, err)
		return
	}

	response.WriteHeader(http.StatusNoContent)
}
