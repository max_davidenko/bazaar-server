package controller

import (
	"bitbucket.org/max_davidenko/bazaar-server/internal/model"
	"bitbucket.org/max_davidenko/bazaar-server/internal/repository"
	"net/http"
	"strings"
)

// GetCustomers is a handler for GET /customers
func (ctrl *Controller) GetCustomers(response http.ResponseWriter, request *http.Request) {
	customers, err := ctrl.repo.GetCustomers()
	writeBasicHeaders(response)
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	customersList := make(map[string][]model.Customer)
	customersList["customers"] = customers
	writeResponse(response, customersList)
}

// GetCustomer is a handler for GET /customers/{id}
func (ctrl *Controller) GetCustomer(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	customerId, err := varAsInt(request, "id", "customer")
	if err != nil {
		writeError(response, err)
		return
	}

	customer, err := ctrl.repo.GetCustomer(uint(customerId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}
	writeResponse(response, customer)
}

// CreateCustomer is a handler for POST /customers
func (ctrl *Controller) CreateCustomer(response http.ResponseWriter, request *http.Request) {

	customer := &model.Customer{}
	if !unmarshalBody(request, response, customer) {
		return
	}

	customer.FirstName = strings.TrimSpace(customer.FirstName)
	if customer.FirstName == "" {
		writeError(response, newBadRequestErrorFromString("customer first name must be specified"))
		return
	}

	customer.MiddleName = strings.TrimSpace(customer.MiddleName)
	if customer.MiddleName == "" {
		writeError(response, newBadRequestErrorFromString("customer middle name must be specified"))
		return
	}

	customer.LastName = strings.TrimSpace(customer.LastName)
	if customer.LastName == "" {
		writeError(response, newBadRequestErrorFromString("customer last name must be specified"))
		return
	}

	customer.Inn = strings.TrimSpace(customer.Inn)
	if customer.Inn == "" {
		writeError(response, newBadRequestErrorFromString("customer INN must be specified"))
		return
	}

	foundCustomer, err := ctrl.repo.FindCustomerByINN(customer.Inn)
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); !isNotFound {
			writeError(response, err)
			return
		}
	}
	if foundCustomer != nil {
		writeError(response, newBadRequestErrorFromString("Customer with specified INN exists"))
		return
	}

	err = ctrl.repo.CreateCustomer(customer)
	if err != nil {
		writeError(response, err)
		return
	}

	writeResponse(response, customer)
}

// EditCustomer is a handler for PUT /customers/{id}
func (ctrl *Controller) EditCustomer(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	customerId, err := varAsInt(request, "id", "customer")
	if err != nil {
		writeError(response, err)
		return
	}

	customer, err := ctrl.repo.GetCustomer(uint(customerId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	updCustomer := &model.Customer{}
	if !unmarshalBody(request, response, updCustomer) {
		return
	}

	updCustomer.ID = customer.ID
	updCustomer.FirstName = strings.TrimSpace(updCustomer.FirstName)
	updCustomer.MiddleName = strings.TrimSpace(updCustomer.MiddleName)
	updCustomer.LastName = strings.TrimSpace(updCustomer.LastName)
	updCustomer.Inn = strings.TrimSpace(updCustomer.Inn)
	if updCustomer.Inn != "" {
		foundCustomer, err := ctrl.repo.FindCustomerByINN(updCustomer.Inn)
		if err != nil {
			if _, isNotFound := err.(*repository.NotFoundError); !isNotFound {
				writeError(response, err)
				return
			}
		}
		if foundCustomer != nil && foundCustomer.ID != updCustomer.ID {
			writeError(response, newBadRequestErrorFromString("Customer with specified INN exists"))
			return
		}
	}

	err = ctrl.repo.UpdateCustomer(updCustomer)
	if err != nil {
		writeError(response, err)
		return
	}

	response.WriteHeader(http.StatusNoContent)
}

// DeleteCustomer is a handler for DELETE /customer/{id}
func (ctrl *Controller) DeleteCustomer(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	customerId, err := varAsInt(request, "id", "customer")
	if err != nil {
		writeError(response, err)
		return
	}

	customer, err := ctrl.repo.GetCustomer(uint(customerId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	err = ctrl.repo.DeleteCustomer(customer)
	if err != nil {
		writeError(response, err)
		return
	}

	response.WriteHeader(http.StatusNoContent)
}
