package controller

import (
	"bitbucket.org/max_davidenko/bazaar-server/internal/model"
	"bitbucket.org/max_davidenko/bazaar-server/internal/repository"
	"net/http"
	"strings"
)

// GetRowPlaces is a handler for GET /rows/{id}/places
func (ctrl *Controller) GetRowPlaces(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	rowId, err := varAsInt(request, "id", "row")
	if err != nil {
		writeError(response, err)
		return
	}

	_, err = ctrl.repo.GetRow(uint(rowId))
	if err != nil {
		if err != nil {
			if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
				writeError(response, newNotFoundError(err))
			} else {
				writeError(response, err)
			}
			return
		}
	}

	places, err := ctrl.repo.GetRowPlaces(uint(rowId))
	writeBasicHeaders(response)
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	placesList := make(map[string][]model.Place)
	placesList["places"] = places
	writeResponse(response, placesList)
}

// GetPlace is a handler for GET /places/{id}
func (ctrl *Controller) GetPlace(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	placeId, err := varAsInt(request, "id", "place")
	if err != nil {
		writeError(response, err)
		return
	}

	place, err := ctrl.repo.GetPlace(uint(placeId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}
	writeResponse(response, place)
}

// CreatePlace is a handler for POST /places
func (ctrl *Controller) CreatePlace(response http.ResponseWriter, request *http.Request) {
	place := &model.Place{}
	if !unmarshalBody(request, response, place) {
		return
	}

	_, err := ctrl.repo.GetRow(uint(place.RowID))
	if err != nil {
		if err != nil {
			if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
				writeError(response, newNotFoundError(err))
			} else {
				writeError(response, err)
			}
			return
		}
	}

	if place.CustomerID != 0 {
		_, err := ctrl.repo.GetCustomer(uint(place.CustomerID))
		if err != nil {
			if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
				writeError(response, newNotFoundError(err))
			} else {
				writeError(response, err)
			}
			return
		}
	}

	place.Name = strings.TrimSpace(place.Name)
	if place.Name == "" {
		writeError(response, newBadRequestErrorFromString("place name must be specified"))
		return
	}

	foundPlace, err := ctrl.repo.FindRowPlaceByName(place.RowID, place.Name)
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); !isNotFound {
			writeError(response, err)
			return
		}
	}
	if foundPlace != nil {
		writeError(response, newBadRequestErrorFromString("Row place with specified name exists"))
		return
	}

	err = ctrl.repo.CreatePlace(place)
	if err != nil {
		writeError(response, err)
		return
	}

	writeResponse(response, place)
}

// EditPlace is a handler for PUT /places/{id}
func (ctrl *Controller) EditPlace(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	placeId, err := varAsInt(request, "id", "place")
	if err != nil {
		writeError(response, err)
		return
	}

	place, err := ctrl.repo.GetPlace(uint(placeId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	updPlace := &model.Place{}
	if !unmarshalBody(request, response, updPlace) {
		return
	}

	updPlace.ID = place.ID
	updPlace.RowID = place.RowID
	updPlace.Name = strings.TrimSpace(updPlace.Name)
	if updPlace.Name != "" {
		foundPlace, err := ctrl.repo.FindRowPlaceByName(updPlace.RowID, updPlace.Name)
		if err != nil {
			if _, isNotFound := err.(*repository.NotFoundError); !isNotFound {
				writeError(response, err)
				return
			}
		}
		if foundPlace != nil && foundPlace.ID != updPlace.ID {
			writeError(response, newBadRequestErrorFromString("Place with specified name exists"))
			return
		}
	}

	err = ctrl.repo.UpdatePlace(updPlace)
	if err != nil {
		writeError(response, err)
		return
	}

	response.WriteHeader(http.StatusNoContent)
}

// DeletePlace is a handler for DELETE /places/{id}
func (ctrl *Controller) DeletePlace(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	placeId, err := varAsInt(request, "id", "place")
	if err != nil {
		writeError(response, err)
		return
	}

	place, err := ctrl.repo.GetPlace(uint(placeId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	err = ctrl.repo.DeletePlace(place)
	if err != nil {
		writeError(response, err)
		return
	}

	response.WriteHeader(http.StatusNoContent)
}

// GetPlacesAttributes is a handler for GET /places/{id}/attributes
func (ctrl *Controller) GetPlacesAttributes(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	placeId, err := varAsInt(request, "id", "place")
	if err != nil {
		writeError(response, err)
		return
	}

	_, err = ctrl.repo.GetPlace(uint(placeId))
	if err != nil {
		if err != nil {
			if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
				writeError(response, newNotFoundError(err))
			} else {
				writeError(response, err)
			}
			return
		}
	}

	attributes, err := ctrl.repo.GetPlaceAttributes(uint(placeId))
	writeBasicHeaders(response)
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	attributesList := make(map[string][]model.PlaceAttribute)
	attributesList["attributes"] = attributes
	writeResponse(response, attributesList)
}

// GetPlaceAttribute is a handler for GET /place/{id}/attributes/{attribute-id}
func (ctrl *Controller) GetPlaceAttribute(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	placeId, err := varAsInt(request, "id", "place")
	if err != nil {
		writeError(response, err)
		return
	}

	attrId, err := varAsInt(request, "attribute-id", "place attribute")
	if err != nil {
		writeError(response, err)
		return
	}

	place, err := ctrl.repo.GetPlace(uint(placeId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	attr, err := ctrl.repo.GetPlaceAttribute(uint(attrId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	if place.ID != attr.PlaceID {
		writeError(response, newBadRequestErrorFromString("attribute with specified id doesn't belong to specified place"))
		return
	}
	writeResponse(response, attr)
}

// CreatePlaceAttribute is a handler for POST /places/{id}/attributes
func (ctrl *Controller) CreatePlaceAttribute(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	placeId, err := varAsInt(request, "id", "place")
	if err != nil {
		writeError(response, err)
		return
	}

	_, err = ctrl.repo.GetPlace(uint(placeId))
	if err != nil {
		if err != nil {
			if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
				writeError(response, newNotFoundError(err))
			} else {
				writeError(response, err)
			}
			return
		}
	}

	attr := &model.PlaceAttribute{}
	if !unmarshalBody(request, response, attr) {
		return
	}
	attr.PlaceID = uint(placeId)

	attr.Name = strings.TrimSpace(attr.Name)
	if attr.Name == "" {
		writeError(response, newBadRequestErrorFromString("attribute name must be specified"))
		return
	}

	foundAttr, err := ctrl.repo.FindPlaceAttributeByName(uint(placeId), attr.Name)
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); !isNotFound {
			writeError(response, err)
			return
		}
	}
	if foundAttr != nil {
		writeError(response, newBadRequestErrorFromString("place already has attribute with specified name"))
		return
	}

	if attr.UnitID == 0 {
		writeError(response, newBadRequestErrorFromString("place attribute unit must be specified"))
		return
	}

	_, err = ctrl.repo.GetUnit(attr.UnitID)
	if err != nil {
		if err != nil {
			if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
				writeError(response, newNotFoundError(err))
			} else {
				writeError(response, err)
			}
			return
		}
	}

	if attr.Value == 0 {
		writeError(response, newBadRequestErrorFromString("place attribute value must be specified"))
		return
	}

	err = ctrl.repo.CreatePlaceAttribute(attr)
	if err != nil {
		writeError(response, err)
		return
	}

	writeResponse(response, attr)
}

// EditPlaceAttribute is a handler for PUT /places/{id}/attributes/{attribute-id}
func (ctrl *Controller) EditPlaceAttribute(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	placeId, err := varAsInt(request, "id", "place")
	if err != nil {
		writeError(response, err)
		return
	}

	attrId, err := varAsInt(request, "attribute-id", "place attribute")
	if err != nil {
		writeError(response, err)
		return
	}

	place, err := ctrl.repo.GetPlace(uint(placeId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	foundAttr, err := ctrl.repo.GetPlaceAttribute(uint(attrId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	if place.ID != foundAttr.PlaceID {
		writeError(response, newBadRequestErrorFromString("attribute with specified id doesn't belong to specified place"))
		return
	}

	updAttr := &model.PlaceAttribute{}
	if !unmarshalBody(request, response, updAttr) {
		return
	}
	updAttr.ID = foundAttr.ID
	updAttr.PlaceID = uint(placeId)
	updAttr.Name = strings.TrimSpace(updAttr.Name)
	if updAttr.Name != "" {
		attrByName, err := ctrl.repo.FindPlaceAttributeByName(uint(placeId), updAttr.Name)
		if err != nil {
			if _, isNotFound := err.(*repository.NotFoundError); !isNotFound {
				writeError(response, err)
				return
			}
		}
		if attrByName != nil && attrByName.ID != updAttr.ID {
			writeError(response, newBadRequestErrorFromString("place already has attribute with specified name"))
			return
		}
	}

	if updAttr.UnitID != 0 {
		_, err = ctrl.repo.GetUnit(updAttr.UnitID)
		if err != nil {
			if err != nil {
				if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
					writeError(response, newNotFoundError(err))
				} else {
					writeError(response, err)
				}
				return
			}
		}
	}

	err = ctrl.repo.UpdatePlaceAttribute(updAttr)
	if err != nil {
		writeError(response, err)
		return
	}

	response.WriteHeader(http.StatusNoContent)
}

// DeletePlaceAttribute is a handler for DELETE /places/{id}/attributes/{attribute-id}
func (ctrl *Controller) DeletePlaceAttribute(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	placeId, err := varAsInt(request, "id", "place")
	if err != nil {
		writeError(response, err)
		return
	}

	attrId, err := varAsInt(request, "attribute-id", "place attribute")
	if err != nil {
		writeError(response, err)
		return
	}

	place, err := ctrl.repo.GetPlace(uint(placeId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	foundAttr, err := ctrl.repo.GetPlaceAttribute(uint(attrId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	if place.ID != foundAttr.PlaceID {
		writeError(response, newBadRequestErrorFromString("attribute with specified id doesn't belong to specified place"))
		return
	}

	err = ctrl.repo.DeletePlaceAttribute(foundAttr)
	if err != nil {
		writeError(response, err)
		return
	}

	response.WriteHeader(http.StatusNoContent)
}

// GetPlacesTariffs is a handler for GET /places/{id}/tariffs
func (ctrl *Controller) GetPlacesTariffs(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	placeId, err := varAsInt(request, "id", "place")
	if err != nil {
		writeError(response, err)
		return
	}

	_, err = ctrl.repo.GetPlace(uint(placeId))
	if err != nil {
		if err != nil {
			if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
				writeError(response, newNotFoundError(err))
			} else {
				writeError(response, err)
			}
			return
		}
	}

	tariffs, err := ctrl.repo.GetPlaceTariffs(uint(placeId))
	writeBasicHeaders(response)
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	tariffsList := make(map[string][]model.PlaceTariff)
	tariffsList["tariffs"] = tariffs
	writeResponse(response, tariffsList)
}

// BindTariffToPlace is a handler for POST /places/{id}/tariffs
func (ctrl *Controller) BindTariffToPlace(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	placeId, err := varAsInt(request, "id", "place")
	if err != nil {
		writeError(response, err)
		return
	}

	_, err = ctrl.repo.GetPlace(uint(placeId))
	if err != nil {
		if err != nil {
			if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
				writeError(response, newNotFoundError(err))
			} else {
				writeError(response, err)
			}
			return
		}
	}

	binding := &model.PlaceTariff{}
	if !unmarshalBody(request, response, binding) {
		return
	}
	if binding.TariffID == 0 {
		writeError(response, newBadRequestErrorFromString("tariff id must be specified"))
		return
	}

	_, err = ctrl.repo.GetTariff(uint(binding.TariffID))
	if err != nil {
		if err != nil {
			if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
				writeError(response, newNotFoundError(err))
			} else {
				writeError(response, err)
			}
			return
		}
	}
	binding.PlaceID = uint(placeId)

	foundBinding, err := ctrl.repo.FindTariffToPlaceBinding(binding.PlaceID, binding.TariffID)
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); !isNotFound {
			writeError(response, err)
			return
		}
	}
	if foundBinding != nil {
		writeError(response, newBadRequestErrorFromString("specified tariff already bound to place"))
		return
	}

	err = ctrl.repo.CreatePlaceTariff(binding)
	if err != nil {
		writeError(response, err)
		return
	}

	writeResponse(response, binding)
}

// UnbindTariffFromPlace is a handler for DELETE /places/{id}/tariffs/{binding-id}
func (ctrl *Controller) UnbindTariffFromPlace(response http.ResponseWriter, request *http.Request) {
	writeBasicHeaders(response)
	placeId, err := varAsInt(request, "id", "place")
	if err != nil {
		writeError(response, err)
		return
	}

	bindingId, err := varAsInt(request, "binding-id", "place tariff")
	if err != nil {
		writeError(response, err)
		return
	}

	place, err := ctrl.repo.GetPlace(uint(placeId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	foundBinding, err := ctrl.repo.GetPlaceTariff(uint(bindingId))
	if err != nil {
		if _, isNotFound := err.(*repository.NotFoundError); isNotFound {
			writeError(response, newNotFoundError(err))
		} else {
			writeError(response, err)
		}
		return
	}

	if place.ID != foundBinding.PlaceID {
		writeError(response, newBadRequestErrorFromString("tariff to place binding with specified id doesn't belong to specified place"))
		return
	}

	err = ctrl.repo.DeletePlaceTariff(foundBinding)
	if err != nil {
		writeError(response, err)
		return
	}

	response.WriteHeader(http.StatusNoContent)
}
