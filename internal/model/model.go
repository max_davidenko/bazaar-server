package model

import (
	"time"
)

type CommonModelFields struct {
	ID        uint       `gorm:"primary_key" json:"ID"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `sql:"index" json:"-"`
}

// Represents the area in the bazaar
type Area struct {
	CommonModelFields
	Name         string `gorm:"type:varchar(128);unique_index"`
	ControllerID uint   `gorm:"index"`
}

// Represents the row in the bazaar area
type Row struct {
	CommonModelFields
	Name   string `gorm:"type:varchar(128);unique_index"`
	AreaID uint   `sql:"type:integer REFERENCES areas(id);index"`
}

// Represents the selling place in the row
type Place struct {
	CommonModelFields
	Name       string   `gorm:"type:varchar(128);unique_index"`
	RowID      uint     `sql:"type:integer REFERENCES rows(id);index"`
	CustomerID uint     `gorm:"index"`
	Tariffs    []Tariff `gorm:"many2many:place_tariffs;"`
	Attributes []PlaceAttribute
	Customer   Customer `gorm:"foreignkey:CustomerID"`
}

// Represents the tariff to pay for selling place
type Tariff struct {
	CommonModelFields
	Name   string `gorm:"unique_index"`
	Value  float32
	UnitID uint
}

// Represents the measure unit
type Unit struct {
	CommonModelFields
	Name string `gorm:"unique_index"`
}

// Represents the attribute of selling place
type PlaceAttribute struct {
	CommonModelFields
	Name    string `gorm:"unique_index"`
	Value   float32
	UnitID  uint `sql:"type:integer REFERENCES units(id)"`
	PlaceID uint `sql:"type:integer REFERENCES places(id)"`
}

// Represents the link between selling place and tariff
type PlaceTariff struct {
	CommonModelFields
	PlaceID  uint `sql:"type:integer REFERENCES places(id);index"`
	TariffID uint `sql:"type:integer REFERENCES tariffs(id);index"`
}

// Represents the selling place's renter
type Customer struct {
	CommonModelFields
	FirstName  string
	MiddleName string
	LastName   string
	Inn        string `gorm:"unique_index"`
}

// Represents the employee that supervises bazaar area
type Controller struct {
	CommonModelFields
	FirstName  string
	MiddleName string
	LastName   string
}

// Items returns model items for repository initialization
func Items() []interface{} {
	return []interface{}{
		&Area{},
		&Row{},
		&Unit{},
		&Place{},
		&Tariff{},
		&PlaceAttribute{},
		&PlaceTariff{},
		&Customer{},
		&Controller{},
	}
}
