package router

// Resource represents http endpoint specification
type Resource struct {
	Name   string
	Method string
	Path   string
}

type Resources []Resource

var resources = Resources{

	// swagger:operation GET /areas Areas areas
	// ---
	// summary: Get all areas
	// description: Returns registered areas list
	// responses:
	//   "200":
	//     "$ref": "#/responses/areasListResponse"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	Resource{
		"Get areas",
		"GET",
		"/areas",
	},

	// swagger:operation GET /areas/{id} Areas areas
	// ---
	// summary: Get area by id
	// description: Returns area with specified id
	// parameters:
	// - name: id
	//   in: path
	//   description: area id
	//   type: integer
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/definitions/Area"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Get area",
		"GET",
		"/areas/{id}",
	},

	// swagger:operation POST /areas Areas areas
	// ---
	// summary: Create new area
	// description: Creates new area with specified parameters
	// parameters:
	// - name: area
	//   in: body
	//   description: area parameters
	//   schema:
	//     "$ref": "#/definitions/createAreaRequest"
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/definitions/Area"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Create area",
		"POST",
		"/areas",
	},

	// swagger:operation PUT /areas/{id} Areas areas
	// ---
	// summary: Edit existing area
	// description: Updates specified area fields
	// parameters:
	// - name: id
	//   in: path
	//   description: area id
	//   type: integer
	//   required: true
	// - name: area
	//   in: body
	//   description: area parameters
	//   schema:
	//     "$ref": "#/definitions/createAreaRequest"
	//   required: true
	// responses:
	//   "204":
	//     "$ref": "#/responses/noContentResponse"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Edit area",
		"PUT",
		"/areas/{id}",
	},

	// swagger:operation DELETE /areas/{id} Areas areas
	// ---
	// summary: Delete area by id
	// description: Deletes area with specified id
	// parameters:
	// - name: id
	//   in: path
	//   description: area id
	//   type: integer
	//   required: true
	// responses:
	//   "204":
	//     "$ref": "#/responses/noContentResponse"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	Resource{
		"Delete area",
		"DELETE",
		"/areas/{id}",
	},

	// swagger:operation GET /areas/{id}/rows Areas areas
	// ---
	// summary: Get area rows
	// description: Returns area rows list
	// parameters:
	// - name: id
	//   in: path
	//   description: area id
	//   type: integer
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/areaRowsListResponse"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Get area rows",
		"GET",
		"/areas/{id}/rows",
	},

	// swagger:operation GET /rows/{id} Rows rows
	// ---
	// summary: Get row by id
	// description: Returns row with specified id
	// parameters:
	// - name: id
	//   in: path
	//   description: row id
	//   type: integer
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/definitions/Row"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Get row",
		"GET",
		"/rows/{id}",
	},

	// swagger:operation POST /rows Rows rows
	// ---
	// summary: Create new area row
	// description: Creates new area row with specified parameters
	// parameters:
	// - name: row
	//   in: body
	//   description: area row parameters
	//   schema:
	//     "$ref": "#/definitions/createRowRequest"
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/definitions/Row"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Add row",
		"POST",
		"/rows",
	},

	// swagger:operation PUT /rows/{id} Rows rows
	// ---
	// summary: Edit existing area row
	// description: Updates specified area row fields
	// parameters:
	// - name: id
	//   in: path
	//   description: row id
	//   type: integer
	//   required: true
	// - name: row
	//   in: body
	//   description: row parameters
	//   schema:
	//     "$ref": "#/definitions/updateRowRequest"
	//   required: true
	// responses:
	//   "204":
	//     "$ref": "#/responses/noContentResponse"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Edit row",
		"PUT",
		"/rows/{id}",
	},

	// swagger:operation DELETE /rows/{id} Rows rows
	// ---
	// summary: Delete area row by id
	// description: Deletes area row with specified id
	// parameters:
	// - name: id
	//   in: path
	//   description: row id
	//   type: integer
	//   required: true
	// responses:
	//   "204":
	//     "$ref": "#/responses/noContentResponse"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	Resource{
		"Delete row",
		"DELETE",
		"/rows/{id}",
	},

	// swagger:operation GET /rows/{id}/places Rows rows
	// ---
	// summary: Get row places
	// description: Returns row places list
	// parameters:
	// - name: id
	//   in: path
	//   description: row id
	//   type: integer
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/rowPlacesListResponse"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Get row places",
		"GET",
		"/rows/{id}/places",
	},

	// swagger:operation GET /places/{id} Places places
	// ---
	// summary: Get place by id
	// description: Returns place with specified id
	// parameters:
	// - name: id
	//   in: path
	//   description: place id
	//   type: integer
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/definitions/Place"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Get place",
		"GET",
		"/places/{id}",
	},

	// swagger:operation POST /places Places places
	// ---
	// summary: Create new row place
	// description: Creates new row place with specified parameters
	// parameters:
	// - name: place
	//   in: body
	//   description: row place parameters
	//   schema:
	//     "$ref": "#/definitions/createPlaceRequest"
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/definitions/Place"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Add place",
		"POST",
		"/places",
	},

	// swagger:operation PUT /places/{id} Places places
	// ---
	// summary: Edit existing row place
	// description: Updates specified row place fields
	// parameters:
	// - name: id
	//   in: path
	//   description: place id
	//   type: integer
	//   required: true
	// - name: place
	//   in: body
	//   description: place parameters
	//   schema:
	//     "$ref": "#/definitions/updatePlaceRequest"
	//   required: true
	// responses:
	//   "204":
	//     "$ref": "#/responses/noContentResponse"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Edit place",
		"PUT",
		"/places/{id}",
	},

	// swagger:operation DELETE /places/{id} Places places
	// ---
	// summary: Delete row place by id
	// description: Deletes row place with specified id
	// parameters:
	// - name: id
	//   in: path
	//   description: place id
	//   type: integer
	//   required: true
	// responses:
	//   "204":
	//     "$ref": "#/responses/noContentResponse"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	Resource{
		"Delete place",
		"DELETE",
		"/places/{id}",
	},

	// swagger:operation GET /tariffs Tariffs tariffs
	// ---
	// summary: Get all tariffs
	// description: Returns tariffs list
	// responses:
	//   "200":
	//     "$ref": "#/responses/tariffsListResponse"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	Resource{
		"Get tariffs",
		"GET",
		"/tariffs",
	},

	// swagger:operation GET /tariffs/{id} Tariffs tariffs
	// ---
	// summary: Get tariff by id
	// description: Returns tariff with specified id
	// parameters:
	// - name: id
	//   in: path
	//   description: tariff id
	//   type: integer
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/definitions/Tariff"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Get tariff",
		"GET",
		"/tariffs/{id}",
	},

	// swagger:operation POST /tariffs Tariffs tariffs
	// ---
	// summary: Create new tariff
	// description: Creates new tariff with specified parameters
	// parameters:
	// - name: tariff
	//   in: body
	//   description: tariff parameters
	//   schema:
	//     "$ref": "#/definitions/createTariffRequest"
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/definitions/Tariff"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Add tariff",
		"POST",
		"/tariffs",
	},

	// swagger:operation PUT /tariffs/{id} Tariffs tariffs
	// ---
	// summary: Edit existing tariff
	// description: Updates specified tariff fields
	// parameters:
	// - name: id
	//   in: path
	//   description: tariff id
	//   type: integer
	//   required: true
	// - name: tariff
	//   in: body
	//   description: tariff parameters
	//   schema:
	//     "$ref": "#/definitions/updateTariffRequest"
	//   required: true
	// responses:
	//   "204":
	//     "$ref": "#/responses/noContentResponse"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Edit tariff",
		"PUT",
		"/tariffs/{id}",
	},

	// swagger:operation DELETE /tariffs/{id} Tariffs tariff
	// ---
	// summary: Delete tariff by id
	// description: Deletes tariff with specified id
	// parameters:
	// - name: id
	//   in: path
	//   description: tariff id
	//   type: integer
	//   required: true
	// responses:
	//   "204":
	//     "$ref": "#/responses/noContentResponse"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	Resource{
		"Delete tariff",
		"DELETE",
		"/tariffs/{id}",
	},

	// swagger:operation GET /units Units units
	// ---
	// summary: Get all units
	// description: Returns units list
	// responses:
	//   "200":
	//     "$ref": "#/responses/unitsListResponse"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	Resource{
		"Get units",
		"GET",
		"/units",
	},

	// swagger:operation GET /units/{id} Units units
	// ---
	// summary: Get unit by id
	// description: Returns unit with specified id
	// parameters:
	// - name: id
	//   in: path
	//   description: unit id
	//   type: integer
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/definitions/Unit"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Get unit",
		"GET",
		"/units/{id}",
	},

	// swagger:operation POST /units Units units
	// ---
	// summary: Create new unit
	// description: Creates new unit with specified parameters
	// parameters:
	// - name: unit
	//   in: body
	//   description: unit parameters
	//   schema:
	//     "$ref": "#/definitions/createUnitRequest"
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/definitions/Unit"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Add unit",
		"POST",
		"/units",
	},

	// swagger:operation PUT /units/{id} Units units
	// ---
	// summary: Edit existing unit
	// description: Updates specified unit fields
	// parameters:
	// - name: id
	//   in: path
	//   description: unit id
	//   type: integer
	//   required: true
	// - name: unit
	//   in: body
	//   description: unit parameters
	//   schema:
	//     "$ref": "#/definitions/updateUnitRequest"
	//   required: true
	// responses:
	//   "204":
	//     "$ref": "#/responses/noContentResponse"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Edit unit",
		"PUT",
		"/units/{id}",
	},

	// swagger:operation DELETE /units/{id} Units units
	// ---
	// summary: Delete unit by id
	// description: Deletes unit with specified id
	// parameters:
	// - name: id
	//   in: path
	//   description: unit id
	//   type: integer
	//   required: true
	// responses:
	//   "204":
	//     "$ref": "#/responses/noContentResponse"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	Resource{
		"Delete unit",
		"DELETE",
		"/units/{id}",
	},

	// swagger:operation GET /places/{id}/attributes Places places
	// ---
	// summary: Get place attributes by place id
	// description: Returns specified place attributes
	// parameters:
	// - name: id
	//   in: path
	//   description: place id
	//   type: integer
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/placeAttributesListResponse"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Get place attributes",
		"GET",
		"/places/{id}/attributes",
	},

	// swagger:operation GET /place/{id}/attributes/{attribute-id} Places places
	// ---
	// summary: Get place attribute by place id
	// description: Returns place attribute with specified id
	// parameters:
	// - name: id
	//   in: path
	//   description: place id
	//   type: integer
	//   required: true
	// - name: attribute-id
	//   in: path
	//   description: place attribute id
	//   type: integer
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/definitions/PlaceAttribute"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Get place attribute",
		"GET",
		"/place/{id}/attributes/{attribute-id}",
	},

	// swagger:operation POST /places/{id}/attributes Places places
	// ---
	// summary: Create new place attribute
	// description: Creates new row place attribute with specified parameters
	// parameters:
	// - name: id
	//   in: path
	//   description: place id
	//   type: integer
	//   required: true
	// - name: place-attribute
	//   in: body
	//   description: place attribute parameters
	//   schema:
	//     "$ref": "#/definitions/createPlaceAttributeRequest"
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/definitions/Place"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Add place attribute",
		"POST",
		"/places/{id}/attributes",
	},

	// swagger:operation PUT /places/{id}/attributes/{attribute-id} Places places
	// ---
	// summary: Edit existing row place attribute
	// description: Updates specified row place attribute fields
	// parameters:
	// - name: id
	//   in: path
	//   description: place id
	//   type: integer
	//   required: true
	// - name: attribute-id
	//   in: path
	//   description: place attribute id
	//   type: integer
	//   required: true
	// - name: place-attribute
	//   in: body
	//   description: place attribute parameters
	//   schema:
	//     "$ref": "#/definitions/updatePlaceAttributeRequest"
	//   required: true
	// responses:
	//   "204":
	//     "$ref": "#/responses/noContentResponse"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Edit place attribute",
		"PUT",
		"/places/{id}/attributes/{attribute-id}",
	},

	// swagger:operation DELETE /places/{id}/attributes/{attribute-id} Places places
	// ---
	// summary: Delete row place attribute by place id
	// description: Deletes row place attribute with specified id
	// parameters:
	// - name: id
	//   in: path
	//   description: place id
	//   type: integer
	//   required: true
	// - name: attribute-id
	//   in: path
	//   description: place attribute id
	//   type: integer
	//   required: true
	// responses:
	//   "204":
	//     "$ref": "#/responses/noContentResponse"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	Resource{
		"Delete place attribute",
		"DELETE",
		"/places/{id}/attributes/{attribute-id}",
	},

	// swagger:operation GET /places/{id}/tariffs Places places
	// ---
	// summary: Get place tariffs by place id
	// description: Returns specified place tariffs
	// parameters:
	// - name: id
	//   in: path
	//   description: place id
	//   type: integer
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/placeTariffsListResponse"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Get place tariffs",
		"GET",
		"/places/{id}/tariffs",
	},

	// swagger:operation POST /places/{id}/tariffs Places places
	// ---
	// summary: Bind tariff to place
	// description: Binds specified tariff to specified place
	// parameters:
	// - name: id
	//   in: path
	//   description: place id
	//   type: integer
	//   required: true
	// - name: place-tariff
	//   in: body
	//   description: place tariff parameters
	//   schema:
	//     "$ref": "#/definitions/bindTariffToPlaceRequest"
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/definitions/PlaceTariff"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Add place tariff",
		"POST",
		"/places/{id}/tariffs",
	},

	// swagger:operation DELETE /places/{id}/tariffs/{binding-id} Places places
	// ---
	// summary: Unbind tariff from place
	// description: Deletes specified tariff binding from place
	// parameters:
	// - name: id
	//   in: path
	//   description: place id
	//   type: integer
	//   required: true
	// - name: binding-id
	//   in: path
	//   description: place tariff id
	//   type: integer
	//   required: true
	// responses:
	//   "204":
	//     "$ref": "#/responses/noContentResponse"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	Resource{
		"Delete place tariff",
		"DELETE",
		"/places/{id}/tariffs/{binding-id}",
	},

	// swagger:operation GET /customers Customers customers
	// ---
	// summary: Get all customers
	// description: Returns customers list
	// responses:
	//   "200":
	//     "$ref": "#/responses/customersListResponse"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	Resource{
		"Get customers",
		"GET",
		"/customers",
	},

	// swagger:operation GET /customers/{id} Customers customers
	// ---
	// summary: Get customer by id
	// description: Returns customer with specified id
	// parameters:
	// - name: id
	//   in: path
	//   description: customer id
	//   type: integer
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/definitions/Customer"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Get customer",
		"GET",
		"/customers/{id}",
	},

	// swagger:operation POST /customers Customers customers
	// ---
	// summary: Create new customer
	// description: Creates new customer with specified parameters
	// parameters:
	// - name: customer
	//   in: body
	//   description: customer parameters
	//   schema:
	//     "$ref": "#/definitions/createCustomerRequest"
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/definitions/Customer"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Add customer",
		"POST",
		"/customers",
	},

	// swagger:operation PUT /customers/{id} Customers customers
	// ---
	// summary: Edit existing customer
	// description: Updates specified customer fields
	// parameters:
	// - name: id
	//   in: path
	//   description: customer id
	//   type: integer
	//   required: true
	// - name: unit
	//   in: body
	//   description: customer parameters
	//   schema:
	//     "$ref": "#/definitions/updateCustomerRequest"
	//   required: true
	// responses:
	//   "204":
	//     "$ref": "#/responses/noContentResponse"
	//   "400":
	//     "$ref": "#/responses/badRequestError"
	Resource{
		"Edit customer",
		"PUT",
		"/customers/{id}",
	},

	// swagger:operation DELETE /customers/{id} Customers customers
	// ---
	// summary: Delete customer by id
	// description: Deletes customer with specified id
	// parameters:
	// - name: id
	//   in: path
	//   description: customer id
	//   type: integer
	//   required: true
	// responses:
	//   "204":
	//     "$ref": "#/responses/noContentResponse"
	//   "404":
	//     "$ref": "#/responses/notFoundError"
	Resource{
		"Delete customer",
		"DELETE",
		"/customers/{id}",
	},

	// Controller
	Resource{
		"Get controllers",
		"GET",
		"/controllers",
	},
	Resource{
		"Get controller",
		"GET",
		"/controllers/{id}",
	},
	Resource{
		"Add controller",
		"POST",
		"/controllers",
	},
	Resource{
		"Edit controller",
		"PUT",
		"/controllers/{id}",
	},
	Resource{
		"Delete controller",
		"DELETE",
		"/controllers/{id}",
	},
}
