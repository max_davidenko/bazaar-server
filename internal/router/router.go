package router

import (
	"bitbucket.org/max_davidenko/bazaar-server/internal/controller"
	"github.com/gorilla/mux"
)

// NewRouter creates new router instance with specified controller implementation
func NewRouter(ctrl *controller.Controller) *mux.Router {
	router := mux.NewRouter().StrictSlash(false)
	routingTable := ctrl.Handlers()
	for _, resource := range resources {
		if handler, exists := routingTable[resource.Name]; exists {
			router.Methods(resource.Method).Path(resource.Path).Name(resource.Name).Handler(handler)
		}
	}

	return router
}
