# bazaar-server

## Description

bazaar-server is a RESTful backend service for bazaar accounting system

## Building with embedded documentation
 
```
make build
```

## API documentation generation (OpenAPI)

```
make api-docs
```

After generation specs will be placed at `api/swagger-spec`

## Installation ($GOBIN)

```
make install
```

## Build and run from source with API documentation generation (swagger is needed)

```
make && make run
```

After running visit http://localhost:8001 to view API specs it the Swagger UI

## Configuration
TODO

## Dependencies
TODO