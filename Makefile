all: api-docs build
build:
	go build -o bin/bazaar-server cmd/bazaar-server/main.go
install:
	go install ./cmd/bazaar-server
run:
	./bin/bazaar-server
api-docs:
	cd cmd/bazaar-server && swagger generate spec -o ../../api/swagger-spec/api.json --scan-models && \
	cp ../../api/swagger-spec/api.json ../../third_party/swagger-ui/swagger.json && \
	cd ../../third_party && go-bindata-assetfs -pkg=swaggerui -o ../web/assets.go swagger-ui/...
