package swagger

import "bitbucket.org/max_davidenko/bazaar-server/internal/model"

// Update area row request
// swagger:model updateRowRequest
type updateRowRequest struct {
	Name string
}

// Create area row request
// swagger:model createRowRequest
type createRowRequest struct {
	Name   string
	AreaID uint
}

// An area rows list
// swagger:response areaRowsListResponse
type areaRowsListResponse struct {
	// Array of row models
	// in:body
	Body struct {
		Rows []model.Row `json:"rows"`
	}
}
