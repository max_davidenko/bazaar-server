package swagger

import "bitbucket.org/max_davidenko/bazaar-server/internal/model"

// A customers list
// swagger:response customersListResponse
type customersListResponse struct {
	// Array of customer models
	// in:body
	Body struct {
		Customers []model.Customer `json:"customers"`
	}
}

// Create customer request
// swagger:model createCustomerRequest
type createCustomerRequest struct {
	FirstName  string
	MiddleName string
	LastName   string
	Inn        string
}

// Update customer request
// swagger:model updateCustomerRequest
type updateCustomerRequest struct {
	FirstName  string
	MiddleName string
	LastName   string
	Inn        string
}
