package swagger

import "bitbucket.org/max_davidenko/bazaar-server/internal/model"

// An areas list
// swagger:response areasListResponse
type areasListResponse struct {
	// Array of area models
	// in:body
	Body struct {
		Areas []model.Area `json:"areas"`
	}
}

// Create area request
// swagger:model createAreaRequest
type createAreaRequest struct {
	Name         string
	ControllerID uint
}
