package swagger

import "bitbucket.org/max_davidenko/bazaar-server/internal/model"

// A row places list
// swagger:response rowPlacesListResponse
type rowPlacesListResponse struct {
	// Array of place models
	// in:body
	Body struct {
		Places []model.Place `json:"places"`
	}
}

// Create row place request
// swagger:model createPlaceRequest
type createPlaceRequest struct {
	Name       string
	RowID      uint
	CustomerID uint
}

// Update row place request
// swagger:model updatePlaceRequest
type updatePlaceRequest struct {
	Name       string
	CustomerID uint
}

// Place attributes list
// swagger:response placeAttributesListResponse
type placeAttributesListResponse struct {
	// Array of place attribute models
	// in:body
	Body struct {
		Attributes []model.PlaceAttribute `json:"attributes"`
	}
}

// Create row place attribute request
// swagger:model createPlaceAttributeRequest
type createPlaceAttributeRequest struct {
	Name   string
	Value  float32
	UnitID uint
}

// Update row place attribute request
// swagger:model updatePlaceAttributeRequest
type updatePlaceAttributeRequest struct {
	Name   string
	Value  float32
	UnitID uint
}

// Place tariffs list
// swagger:response placeTariffsListResponse
type placeTariffsListResponse struct {
	// Array of place tariff models
	// in:body
	Body struct {
		Tariffs []model.PlaceTariff `json:"tariffs"`
	}
}

// Bind tariff to place request
// swagger:model bindTariffToPlaceRequest
type bindTariffToPlaceRequest struct {
	TariffID uint
}
