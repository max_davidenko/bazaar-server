package swagger

import "bitbucket.org/max_davidenko/bazaar-server/internal/model"

// Units list
// swagger:response unitsListResponse
type unitsListResponse struct {
	// Array of unit models
	// in:body
	Body struct {
		Units []model.Unit `json:"units"`
	}
}

// Create unit request
// swagger:model createUnitRequest
type createUnitRequest struct {
	Name string
}

// Update unit request
// swagger:model updateUnitRequest
type updateUnitRequest struct {
	Name string
}
