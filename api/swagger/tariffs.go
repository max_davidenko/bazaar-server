package swagger

import "bitbucket.org/max_davidenko/bazaar-server/internal/model"

// Tariffs list
// swagger:response tariffsListResponse
type tariffsListResponse struct {
	// Array of tariff models
	// in:body
	Body struct {
		Tariffs []model.Tariff `json:"tariffs"`
	}
}

// Create tariff request
// swagger:model createTariffRequest
type createTariffRequest struct {
	Name   string
	Value  float32
	UnitID uint
}

// Update tariff request
// swagger:model updateTariffRequest
type updateTariffRequest struct {
	Name   string
	Value  float32
	UnitID uint
}
