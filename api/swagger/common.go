package swagger

// Not Found
// swagger:response notFoundError
type notFoundError struct {
	// in:body
	Body struct {
		// Detailed error message
		Message string `json:"message"`
	}
}

// Bad Request
// swagger:response badRequestError
type badRequestError struct {
	// in:body
	Body struct {
		// Detailed error message
		Message string `json:"message"`
	}
}

// An empty response for no content
// swagger:response noContentResponse
type noContentResponse struct{}
