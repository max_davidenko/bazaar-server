// bazaar server
//
// bazaar server is a backend for bazaar accounting system. This documentation describes its APIs
//
//     Schemes: http
//     BasePath: /
//     Version: 0.1.0
//     License: MIT http://opensource.org/licenses/MIT
//     Contact: Max Davidenko <armwebdev@gmail.com>
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Security:
//     - bearer
//
//     SecurityDefinitions:
//     bearer:
//          type: apiKey
//          name: Authorization
//          in: header
//
// swagger:meta
package main

import (
	_ "bitbucket.org/max_davidenko/bazaar-server/api/swagger"
	"bitbucket.org/max_davidenko/bazaar-server/internal/controller"
	"bitbucket.org/max_davidenko/bazaar-server/internal/model"
	"bitbucket.org/max_davidenko/bazaar-server/internal/repository"
	"bitbucket.org/max_davidenko/bazaar-server/internal/router"
	"bitbucket.org/max_davidenko/bazaar-server/web"
	"flag"
	"fmt"
	"github.com/gorilla/handlers"
	"log"
	"net/http"
	"os"
	"strconv"
)

const (
	defaultDbUser    = "bazaar_user"
	defaultDbPass    = "bazaar_pass"
	defaultDbHost    = "localhost:5432"
	defaultDbName    = "bazaar"
	defaultDbSslMode = "disable"
	defaultDbLogMode = "true"
	defaultHttpPort  = 8001
)

func main() {
	dbHost := flag.String("db-host", defaultDbHost, "Database host (localhost:5432)")
	dbName := flag.String("db-name", defaultDbName, "Database name (bazaar)")
	dbUser := flag.String("db-user", defaultDbUser, "Database user (bazaar_user)")
	dbPass := flag.String("db-pass", defaultDbPass, "Database password (bazaar_pass)")
	dbSslMode := flag.String("db-sslmode", defaultDbSslMode, "Ssl mode for postgres connection (disable)")
	dbLogMode := flag.String("db-logmode", defaultDbLogMode, "Log mode for database queries (true)")
	httpPort := flag.Int("port", defaultHttpPort, "Service  port (8001)")

	help := flag.Bool("help", false, "Prints this help")

	flag.Parse()

	if *help {
		flag.PrintDefaults()
		os.Exit(0)
	}

	repoParams := map[string]string{
		"repoName": "postgres",
		"user":     *dbUser,
		"password": *dbPass,
		"host":     *dbHost,
		"db":       *dbName,
		"sslmode":  *dbSslMode,
		"logmode":  *dbLogMode,
	}

	repo, err := repository.CreateRepository(repoParams)
	if err != nil {
		fmt.Println("Repository creation error: " + err.Error()) // TODO log
		os.Exit(1)
	}

	err = repo.InitModel(model.Items()...)
	if err != nil {
		fmt.Println("Model initialization error: " + err.Error()) // TODO log
		os.Exit(1)
	}

	appController := controller.NewController(repo)
	appRouter := router.NewRouter(appController)

	appRouter.PathPrefix("/swagger-ui/").
		Handler(http.StripPrefix("/swagger-ui/", swaggerui.GetFS()))

	appRouter.PathPrefix("/").
		Handler(http.StripPrefix("/", swaggerui.GetFS()))

	// TODO read about
	allowedOrigins := handlers.AllowedOrigins([]string{"*"})
	allowedMethods := handlers.AllowedMethods([]string{"GET", "POST", "DELETE", "PUT"})

	log.Fatal(http.ListenAndServe(":"+strconv.Itoa(*httpPort), handlers.CORS(allowedOrigins, allowedMethods)(appRouter)))
}
