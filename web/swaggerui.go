package swaggerui

import (
	"net/http"
)

func GetFS() http.Handler {
	return http.FileServer(assetFS())
}
